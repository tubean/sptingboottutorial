package com.tuda.springbootrap.config;

import com.tuda.springbootrap.config.exception.BookIdMismatchException;
import com.tuda.springbootrap.config.exception.BookNotFoundException;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .anyRequest()
                .permitAll()
                .and().csrf().disable();
    }

    @ControllerAdvice
    public static class RestExceptionHandler extends ResponseEntityExceptionHandler {

      @ExceptionHandler({BookNotFoundException.class})
      protected ResponseEntity<Object> handleNotFound(Exception ex, WebRequest request) {
        return handleExceptionInternal(
            ex, "Book not found", new HttpHeaders(), HttpStatus.NOT_FOUND, request);
      }

      @ExceptionHandler({
        BookIdMismatchException.class,
        ConstraintViolationException.class,
        DataIntegrityViolationException.class
      })
      public ResponseEntity<Object> handleBadRequest(Exception ex, WebRequest request) {
        return handleExceptionInternal(
            ex, ex.getLocalizedMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
      }
    }
}
