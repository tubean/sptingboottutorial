package com.tuda.springbootrap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("com.tuda.springbootrap.repository")
@EntityScan("com.tuda.springbootrap.entity")
@SpringBootApplication
public class SpringWithBootrapApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringWithBootrapApplication.class, args);
  }
}
