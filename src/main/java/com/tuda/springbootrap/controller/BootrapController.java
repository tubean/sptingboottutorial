package com.tuda.springbootrap.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class BootrapController {
  @Value("${spring.application.name}")
  String appName;

  @RequestMapping("/")
  public String homePage(Model model) {
    model.addAttribute("appName", appName);
    return "home";
  }

  @RequestMapping("/actuator")
  public String actuatorPage(HttpServletRequest request, Model model) {
    String contextPath = request.getContextPath();
    String host = request.getServerName();

    // Spring Boot >= 2.0.0.M7
    String endpointBasePath = "/actuator";

    // http://localhost:8090/actuator
    String url = "http://" + host + ":8090" + contextPath + endpointBasePath;

    model.addAttribute("data",url);
    return "actuator";
  }
}
